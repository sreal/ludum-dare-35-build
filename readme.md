ludum-dare-35
======

**ludum-dare-35** is a ludum dare 35 game. #LDJAM

## Download

### Builds

* [windows-build](https://bitbucket.org/sreal/ludum-dare-35-build/get/windows.tar.gz)
* [osx-build](https://bitbucket.org/sreal/ludum-dare-35-build/get/osx.tar.gz)
* [linux-build](https://bitbucket.org/sreal/ludum-dare-35-build/get/linux.tar.gz)

### Code

* [master](https://bitbucket.org/sreal/ludum-dare-35/get/master.tar.gz)


## Usage
    $ git clone git@bitbucket.org:sreal/ludum-dare-35.git

## Version
* Version 0.0.1

## License
* See [MIT License](license.md)

## Thanks
* See [Thanks and attribution](attribution.md)

## Contact
* Homepage: http://sreal.github.io/jekyll
* e-mail: simon.richard.eames@gmail.com
* Twitter: [@sreal](https://twitter.com/sreal "sreal on twitter")

----
[![Flattr this git repo](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=sreal&url=https://bitbucket.org/sreal/ludum-dare-35)
